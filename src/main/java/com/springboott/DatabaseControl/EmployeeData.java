package com.springboott.DatabaseControl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.springboott.model.Employee;
import com.springboott.repository.EmployeeRepository;

@Service
public class EmployeeData {
 
	@Autowired
	EmployeeRepository employeeRepository;

	//save
	public Employee save(Employee emp) {
		return	employeeRepository.save(emp);
	}
	//search
	public List<Employee> findAll() {
		return employeeRepository.findAll();
	}
	
	//get
	public Employee findOne(Long empid) {
	
		return employeeRepository.findById(empid).orElse(null); //findOne();
	}/*
	
	public Employee findOne(Long empid) {
		 
		cp = employeeRepository.findById(empid);
		   return cp.orElseThrow;
	}
		*/
	
	
	//delete
	public void delete(Employee emp) {
		 employeeRepository.delete(emp);
	}
	


}
